package com.example.shoutinkotlin

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.post_submission.view.*

class SubmissionsAdapter(private val clickListener: (ShoutSubmission) -> Unit) : RecyclerView.Adapter<SubmissionsAdapter.ViewHolder>() {

    var subList : List<ShoutSubmission> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.post_submission,parent,false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return subList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, p1: Int) {
        val shoutSubmission : ShoutSubmission = subList[p1]

        holder.bind(shoutSubmission,clickListener)
    }

    fun setSubmissionsList(subsList : List<ShoutSubmission>){
        this.subList = subsList
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        fun bind(item : ShoutSubmission, clickListener: (ShoutSubmission) -> Unit){
            itemView.Logintext.text = item.login
            itemView.datetext.text = item.date
            itemView.contenttext.text = item.content
            itemView.setOnClickListener {clickListener(item)}
        }
    }


}