package com.example.shoutinkotlin

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.*


interface ShoutAPI {

    @GET("/shoutbox/messages")
    fun getSubmissions(): Call<List<ShoutSubmission>>

    @POST("shoutbox/message")
    fun postSubmission(@Body n: NewSubmission) : Call<ShoutSubmission>

    @PUT("shoutbox/message/{id}")
    fun editSubmission(@Path("id") id : String, @Body n: NewSubmission) : Call<ShoutSubmission>

    @DELETE("shoutbox/message/{id}")
    fun deleteSubmission(@Path("id") id : String) : Call<ShoutSubmission>

    


    companion object {
        fun create(): ShoutAPI{
            val retrofit = Retrofit.Builder().baseUrl( "http://tgryl.pl")
                .addConverterFactory(GsonConverterFactory.create()).build()
            return retrofit.create(ShoutAPI::class.java)
        }
    }
}
