package com.example.shoutinkotlin

import com.google.gson.annotations.SerializedName


data class NewSubmission(
    @SerializedName("content") var content: String,
    @SerializedName("login") var login: String
)