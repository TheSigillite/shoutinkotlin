package com.example.shoutinkotlin

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_post_message.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.Serializable

class PostMessage : AppCompatActivity() {

    lateinit var login : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_message)
        login = intent.getStringExtra("log")
        currentlog.text = login
        PostButton.setOnClickListener{
            yeetMessage()
        }
    }

    fun yeetMessage(){
        ShoutAPI.create().postSubmission(NewSubmission(editText2.text.toString(),login)).enqueue(object : Callback<ShoutSubmission>{
            override fun onResponse(call: Call<ShoutSubmission>, response: Response<ShoutSubmission>) {
                if(response.code() == 200){
                    val inte = Intent()
                    inte.putExtra("Bruh",200)
                    setResult(Activity.RESULT_OK,inte)
                    finish()
                }
                else{
                    val inte = Intent()
                    inte.putExtra("Bruh",404)
                    setResult(Activity.RESULT_OK,inte)
                    finish()
                }
            }

            override fun onFailure(call: Call<ShoutSubmission>, t: Throwable) {
                    val xd = Intent()
                    xd.putExtra("pepega",t)
                    setResult(Activity.RESULT_CANCELED,xd)
                    finish()
            }
        })

    }

    override fun onBackPressed() {
        val inte = Intent()
        inte.putExtra("Bruh",404)
        setResult(Activity.RESULT_OK,inte)
        finish()
    }
}
