package com.example.shoutinkotlin

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.widget.LinearLayout
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_shoutbox_view.*
import kotlinx.android.synthetic.main.app_bar_shoutbox_view.*
import kotlinx.android.synthetic.main.content_shoutbox_view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ShoutboxView : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    lateinit var login : String
    var subs : List<ShoutSubmission> = emptyList()
    lateinit var recyclerView: RecyclerView
    lateinit var recyclerAdapter: SubmissionsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shoutbox_view)
        setSupportActionBar(toolbar)


        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        login = intent.getStringExtra("login")

        getNewMessages()
        recyclerView = SubRecycler
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerAdapter = SubmissionsAdapter {item -> itemClicked(item)}
        recyclerView.adapter = recyclerAdapter
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.shoutbox_view, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_settings -> {
                finish()
            }
            R.id.nav_shoutbox -> {
                getNewMessages()
            }
            R.id.new_msg -> {
                val tonew = Intent(applicationContext,PostMessage::class.java)
                tonew.putExtra("log",login)
                startActivityForResult(tonew,42)
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    fun getNewMessages(){
        var shoutAPI = ShoutAPI.create()
        var submissions = shoutAPI.getSubmissions()
        submissions.enqueue(object : Callback<List<ShoutSubmission>> {
            override fun onFailure(call: Call<List<ShoutSubmission>>, t: Throwable) {
                val context = applicationContext
                val errtoast = Toast.makeText(context,"nie udało sie pobrać zawartości"+t.toString(),Toast.LENGTH_LONG)
                errtoast.setGravity(Gravity.TOP,0,0)
                errtoast.show()
            }

            override fun onResponse(call: Call<List<ShoutSubmission>>, response: Response<List<ShoutSubmission>>) {
                if (response.code()==200){
                    subs = response.body()!!
                    recyclerAdapter.setSubmissionsList(subs)
                }
                else{
                    val context = applicationContext
                    val errtoast = Toast.makeText(context,"nie udało sie pobrać zawartości"+response.code(),Toast.LENGTH_SHORT)
                    errtoast.setGravity(Gravity.TOP,0,0)
                    errtoast.show()
                }

            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        var responsetoast = Toast.makeText(this,"",Toast.LENGTH_LONG)
        responsetoast.setGravity(0,0,Gravity.TOP)
        if(requestCode==42 && resultCode== Activity.RESULT_OK){
            var result = data!!.getIntExtra("Bruh",404)
            if(result==200){
                responsetoast.setText("Post Sent Succesfully")
                getNewMessages()
                responsetoast.show()
            }
            else if(result == 404){
                responsetoast.setText("Unable to sent Post. Try again later")
                responsetoast.show()
            }
        }
        else if(requestCode==42 && resultCode== Activity.RESULT_CANCELED){
            var err : Throwable = data!!.getSerializableExtra("pepega") as Throwable
            responsetoast.setText(err.toString())
            responsetoast.show()
        }

        if(requestCode==69 && resultCode==Activity.RESULT_OK){
            var result = data!!.getIntExtra("B-Emoji",404)
            if(result==200){
                responsetoast.setText("Post edited successfully!")
                responsetoast.show()
                getNewMessages()
            }
            else if(result==201){
                responsetoast.setText("Post removed successfully!")
                responsetoast.show()
                getNewMessages()
            }
            else if(result==404){
                responsetoast.setText("Unable to Edit/Delete. Try again later")
                responsetoast.show()
            }
        }
        else if(requestCode==69 && resultCode==Activity.RESULT_CANCELED){
            val err : Throwable = data!!.getSerializableExtra("omegalul") as Throwable
            responsetoast.setText(err.toString())
            responsetoast.show()
        }
    }

    private fun itemClicked(Item : ShoutSubmission){
        var clicktoast = Toast.makeText(this, Item.id, Toast.LENGTH_SHORT)
        clicktoast.setGravity(0,0,Gravity.TOP)
        clicktoast.show()

        val toedit = Intent(applicationContext,EditRemoveActivity::class.java)
        toedit.putExtra("logi",Item.login)
        toedit.putExtra("cont",Item.content)
        toedit.putExtra("pass",Item.id)
        startActivityForResult(toedit,69)
    }
}
