package com.example.shoutinkotlin

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_edit_remove.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EditRemoveActivity : AppCompatActivity() {
    lateinit var login : String
    lateinit var content : String
    lateinit var id : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_remove)
        login = intent.getStringExtra("logi")
        content = intent.getStringExtra("cont")
        id = intent.getStringExtra("id")
        val tologin : String = "Currently logged as : "+login
        currentlogin.text = tologin
        editedMessage.setText(content)
        checkBox.setOnCheckedChangeListener { buttonView, isChecked ->
            deleteButton.isEnabled = isChecked
            deleteButton.isClickable = isChecked
        }
        editButton.setOnClickListener(){
            editSub()
        }
        deleteButton.setOnClickListener(){
            deleteSub()
        }
    }


    fun deleteSub(){
        ShoutAPI.create().deleteSubmission(id).enqueue(object : Callback<ShoutSubmission>{

            override fun onResponse(call: Call<ShoutSubmission>, response: Response<ShoutSubmission>) {
                if(response.code() == 200){
                    val intentual = Intent()
                    intentual.putExtra("B-Emoji",200)
                    setResult(Activity.RESULT_OK,intentual)
                    finish()
                }
                else{
                    val intentual = Intent()
                    intentual.putExtra("B-Emoji",404)
                    setResult(Activity.RESULT_OK,intentual)
                    finish()
                }
            }

            override fun onFailure(call: Call<ShoutSubmission>, t: Throwable) {
                val xdd = Intent()
                xdd.putExtra("omegalul",t)
                setResult(Activity.RESULT_CANCELED,xdd)
                finish()
            }
        })
    }

    fun editSub(){
        ShoutAPI.create().editSubmission(id, NewSubmission(editedMessage.text.toString(),login)).enqueue(object : Callback<ShoutSubmission>{
            override fun onResponse(call: Call<ShoutSubmission>, response: Response<ShoutSubmission>) {
                if(response.code() == 200){
                    val intentual = Intent()
                    intentual.putExtra("B-Emoji",201)
                    setResult(Activity.RESULT_OK,intentual)
                    finish()
                }
                else{
                    val intentual = Intent()
                    intentual.putExtra("B-Emoji",404)
                    setResult(Activity.RESULT_OK,intentual)
                    finish()
                }
            }

            override fun onFailure(call: Call<ShoutSubmission>, t: Throwable) {
                val xdd = Intent()
                xdd.putExtra("omegalul",t)
                setResult(Activity.RESULT_CANCELED,xdd)
                finish()
            }
        })
    }

    override fun onBackPressed(){
        val intentual = Intent()
        intentual.putExtra("B-Emoji",404)
        setResult(Activity.RESULT_OK,intentual)
        finish()
    }
}
