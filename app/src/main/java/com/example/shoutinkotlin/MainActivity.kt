package com.example.shoutinkotlin

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val sharedp = getSharedPreferences("shared_pref", Context.MODE_PRIVATE)
        val loginfo = sharedp.getString("LoginS","")
        if (!loginfo.equals("")){
            val passer = Intent(applicationContext,ShoutboxView::class.java);
            passer.putExtra("login",loginfo)
            startActivity(passer)
        }
        val loginbutton = findViewById<Button>(R.id.logi)
        loginbutton.setOnClickListener(){
            val texbox = findViewById<EditText>(R.id.editText)
            var login = texbox.getText().toString()
            if (!login.equals("")){
                val sharedpreferences = getSharedPreferences("shared_pref", Context.MODE_PRIVATE)
                val sharededitor = sharedpreferences.edit()
                sharededitor.putString("LoginS",login)
                sharededitor.apply()
                val passer = Intent(applicationContext,ShoutboxView::class.java);
                passer.putExtra("login",login)
                startActivity(passer)
            }
        }
    }

}
