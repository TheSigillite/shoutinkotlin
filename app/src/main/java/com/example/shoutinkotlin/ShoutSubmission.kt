package com.example.shoutinkotlin

import com.google.gson.annotations.SerializedName

class ShoutSubmission(
    @SerializedName("content") var content: String,
    @SerializedName("login") var login: String,
    @SerializedName("date") var date: String,
    @SerializedName("id") var id: String
)